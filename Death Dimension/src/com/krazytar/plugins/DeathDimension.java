package com.krazytar.plugins;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class DeathDimension extends JavaPlugin implements Listener {
    double chance;
    ArrayList<String> enabledWorlds;
    World deathDimension;
    Location entryPoint;
    FileConfiguration config;
    boolean isSetUp = true;
    File dataFile = null;
    FileConfiguration data;
    
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        config = getConfig();
        getDataFile();
        config.options().copyDefaults(true);
        saveDefaultConfig();
        if(config.contains("world") || config.contains("position") || !config.contains("worlds")) {
            updateConfig();
        }
        chance = config.getDouble("chance");
        try {
            deathDimension = getServer().getWorld(data.getString("world"));
            entryPoint = new Location(deathDimension, data.getDouble("position.x"), data.getDouble("position.y"), data.getDouble("position.z"));
        } catch(IllegalArgumentException exc) {
            deathDimension = null;
            isSetUp = false;
        }
        enabledWorlds = (ArrayList) config.getList("worlds");
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(label.equalsIgnoreCase("dd")) {
            if((args.length > 0)) {
                if(sender instanceof Player) {
                    Player player = (Player) sender;
                    if(args[0].equalsIgnoreCase("set")) {
                        entryPoint = player.getLocation();
                        isSetUp = true;
                        player.sendMessage(ChatColor.GREEN + "Entry Point set!");
                        saveEntryPoint();
                        return true;
                    } else if(args[0].equalsIgnoreCase("teleport")) {
                        teleportToDeathDimension(player);
                        return true;
                    } else if(args[0].equalsIgnoreCase("addworld")) {
                        if(enabledWorlds == null) {
                            ArrayList<String> worldBuf = (ArrayList) config.getStringList("worlds");
                            worldBuf.add(player.getWorld().getName());
                            config.set("worlds", worldBuf);
                            saveConfig();
                            enabledWorlds = (ArrayList) config.getList("worlds");
                            player.sendMessage(ChatColor.GREEN + "The world " + player.getWorld().getName() + " has been enabled.");
                            return true;
                        }
                        if(enabledWorlds.contains(player.getWorld().getName())) {
                            sender.sendMessage(ChatColor.RED + "The world" + player.getWorld().getName() + " is already enabled!");
                            return true;
                        }
                        enabledWorlds.add(player.getWorld().getName());
                        config.set("worlds", enabledWorlds);
                        saveConfig();
                        player.sendMessage(ChatColor.GREEN + "The world " + player.getWorld().getName() + " has been enabled.");
                        return true;
                    } else if(args[0].equalsIgnoreCase("delworld")) {
                        if(enabledWorlds == null) {
                            player.sendMessage(ChatColor.RED + "The world " + player.getWorld().getName() + " cannot be unenabled because it hasn't been enabled!");
                            return true;
                        }
                        if(!enabledWorlds.contains(player.getWorld().getName())) {
                            player.sendMessage(ChatColor.RED + "The world " + player.getWorld().getName() + " cannot be unenabled because it hasn't been enabled!");
                            return true;
                        }
                        enabledWorlds.remove(player.getWorld().getName());
                        config.set("worlds", enabledWorlds);
                        saveConfig();
                        player.sendMessage(ChatColor.GREEN + "The world " + player.getWorld().getName() + " is no longer enabled!");
                        return true;
                    }
                }
            } else {
                sender.sendMessage(ChatColor.GREEN + "This plugin was created by Javy_K (KrazyRaven)");
                sender.sendMessage(ChatColor.GOLD + "---Commands---");
                sender.sendMessage(ChatColor.GREEN + "/dd - The base command");
                sender.sendMessage(ChatColor.GREEN + "/dd set - Sets the Death Dimension world and spawn point");
                sender.sendMessage(ChatColor.GREEN + "/dd teleport - Teleports you to the Death Dimension");
                sender.sendMessage(ChatColor.GREEN + "/dd addworld - Enables the world you are currently in");
                sender.sendMessage(ChatColor.GREEN + "/dd delworld - Deletes the world you are currently in");
            }
        }
        return true;
    }
    
    void saveEntryPoint() {
        data.set("world", entryPoint.getWorld().getName());
        data.set("position.x", entryPoint.getX());
        data.set("position.y", entryPoint.getY());
        data.set("position.z", entryPoint.getZ());
        saveDataFile();
    }
    
    boolean willBeDead(double health, double damage) {
        if(health - damage <= 0) {
            return true;
        }
        return false;
    }
    
    boolean checkChance() {
        int rand = (int) Math.round(Math.random() * 100 +1);
        if(rand <= chance) {
            return true;
        }
        return false;
    }
    
    void teleportToDeathDimension(Player player) {
        if(isSetUp) {
            player.teleport(entryPoint);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', config.getString("teleport-message")));
            player.setHealth(20);
        }   else{
            player.sendMessage(ChatColor.RED + "Death Dimension is not set up! Please set the entry point with the command /dd set");
        }
    }
    
    FileConfiguration getDataFile() {
        if(dataFile == null) {
            dataFile = new File(getDataFolder(), "data.yml");
            data = YamlConfiguration.loadConfiguration(dataFile);
        }
        return data;
    }
    
    boolean saveDataFile() {
        if(dataFile == null || data == null) {
            return false;
        }
        try {
            getDataFile().save(dataFile);
        } catch(IOException ioe) {
            getLogger().info("Could not save data!");
            return false;
        }
        return true;
    }
    
    void updateConfig() {
        getLogger().info("Outdated config found. Updating...");
        String world;
        double x, y, z;
        world = config.getString("world");
        x = config.getDouble("position.x");
        y = config.getDouble("position.y");
        z = config.getDouble("position.z");
        data.set("world", world);
        data.set("position.x", x);
        data.set("position.y", y);
        data.set("position.z", z);
        config.set("world", null);
        config.set("position", null);
        config.set("worlds", "");
        if(saveDataFile()) {
            getLogger().info("Config update successful!");
        } else {
            getLogger().info("Config update failed.");
        }
        saveConfig();
    }
    
    @EventHandler
    void onEntityDamaged(EntityDamageEvent e) {
        Entity p = e.getEntity();
        if(!(p instanceof Player)) {
            return;
        }
        Player player = (Player) e.getEntity();
        if(player.hasPermission("deathdimension.immune")) {
            return;
        }
        if(enabledWorlds != null) {
            if(!enabledWorlds.contains(p.getWorld().getName())) {
                return;
            }
        } else {
            return;
        }
        if(!willBeDead(player.getHealth(), e.getDamage())) {
            return;
        }
        if(checkChance()) {
            teleportToDeathDimension((Player) e.getEntity());
            e.setCancelled(true);
        }
    }
}
